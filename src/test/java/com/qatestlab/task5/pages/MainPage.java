package com.qatestlab.task5.pages;

import com.qatestlab.task5.base.BasePage;
import com.qatestlab.task5.model.ProductData;
import com.qatestlab.task5.utils.DataConverter;
import com.qatestlab.task5.utils.logging.CustomReporter;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

import static com.qatestlab.task5.utils.Properties.getBaseUrl;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MainPage extends BasePage {

    private int id;
    private ProductData product = new ProductData();

    @FindBy(how = How.XPATH, using = "//section[@id='content']/section/a")
    private WebElement allProducts;

    private MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public static MainPage init(WebDriver driver) {
        return new MainPage(driver);
    }

    public MainPage launch() {
        driver.get(getBaseUrl());
        return this;
    }

    public boolean isMobileSite() {
        return driver
                .findElement(By.xpath("//*[@class='hidden-md-up text-xs-center mobile']"))
                .isDisplayed();
    }

    public MainPage openRandomProduct() {
        CustomReporter.logAction("Opening random product");
        waitElementToBeClickable(allProducts).click();
        Random random = new Random();
        List<WebElement> products = driver.findElements(By.xpath("//article[*]//h1/a"));
        id = random.nextInt(products.size() - 1) + 1;
        product.setName(waitElementVisibility(By.xpath("//article[" + id + "]//h1/a")).getText());
        waitElementToBeClickable(By.xpath("//article[" + id + "]//h1/a")).click();
        return this;
    }

    public MainPage saveOpenedProduct() {
        CustomReporter.logAction("Getting information about currently opened product");
        float price = DataConverter.parsePriceValue(
                waitElementPresence(By.cssSelector(".current-price>span")).getText());
        waitElementToBeClickable(By.xpath("//a[contains(@href, '#product-details')]")).click();
        int qty = DataConverter.parseStockValue(
                waitElementToBeClickable(By.cssSelector(".product-quantities>span")).getText());
        product.setQty(qty);
        product.setPrice(price);
        return this;
    }

    public MainPage addProductToCart() {
        CustomReporter.logAction("Adding product to Cart");
        waitElementToBeClickable(By.xpath("(//button[@type='submit'])[2]")).click();
        waitElementToBeClickable(By.cssSelector("a.btn.btn-primary")).click();
        CustomReporter.logAction("Validating product information in the Cart");
        String name = waitElementVisibility(By.cssSelector("a.label:nth-child(1)")).getText();
        assertEquals(name, product.getName(), "Wrong product name detected in the cart");
        float price = DataConverter.parsePriceValue(waitElementVisibility(By.cssSelector("span.value")).getText());
        assertEquals(price, product.getPrice(),
                "Product price in the cart doesn't correspond to the price on the product page");
        return this;
    }

    public MainPage createOrder() {
        CustomReporter.logAction("Proceeding to order creation");
        waitElementToBeClickable(By.cssSelector("a.btn")).click();
        return this;
    }

    public MainPage fillOrderInformation(JSONObject dataset) {
        CustomReporter.logAction("Filling required information");
        waitElementVisibility(By.name("firstname")).sendKeys(dataset.getString("firstname"));
        waitElementVisibility(By.name("lastname")).sendKeys(dataset.getString("lastname"));
        waitElementVisibility(By.name("email")).sendKeys(dataset.getString("email"));
        waitElementToBeClickable(By.name("continue")).click();
        waitElementVisibility(By.name("address1")).sendKeys(dataset.getString("address1"));
        waitElementVisibility(By.name("postcode")).sendKeys(dataset.getString("postcode"));
        waitElementVisibility(By.name("city")).sendKeys(dataset.getString("city"));
        return this;
    }

    public MainPage placeOrder() {
        CustomReporter.logAction("Placing new order");
        waitElementToBeClickable(By.name("confirm-addresses")).click();
        waitElementToBeClickable(By.name("confirmDeliveryOption")).click();
        waitElementPresence(By.id("payment-option-2")).click();
        waitElementPresence(By.id("conditions_to_approve[terms-and-conditions]")).click();
        waitElementToBeClickable(By.cssSelector(".btn.btn-primary.center-block")).click();
        return this;
    }

    public MainPage validateOrder() {
        CustomReporter.logAction("Validating order summary");
        String alert = waitElementVisibility(By.cssSelector("h3.h1.card-title")).getText().trim().toLowerCase();
        assertTrue(alert.contains("��� ����� ����������"), "Order is not confirmed");
        int items = Integer.parseInt(waitElementPresence(By.cssSelector("div.col-xs-2")).getText());
        assertEquals(items, 1, "Wrong number of items ordered");
        String nameContext = waitElementPresence((By.xpath("//div[@id='order-items']//div[2]/span"))).getText();
        String name = nameContext.substring(0, product.getName().length());
        float price = DataConverter.parsePriceValue(
                waitElementPresence(By.cssSelector("div.col-xs-5:nth-child(1)")).getText());
        assertEquals(name, product.getName(), "Product name doesn't match");
        assertEquals(price, product.getPrice(), "Final product price is wrong");
        return this;
    }

    public MainPage checkUpdatedStock() {
    CustomReporter.logAction("Checking updated In Stock value");
        waitElementToBeClickable(By.cssSelector("img.logo.img-responsive")).click();
        waitElementToBeClickable(By.xpath("//section[@id='content']/section/a")).click();
        waitElementToBeClickable(By.xpath("//article[" + id + "]//h1/a")).click();
        waitElementToBeClickable(By.xpath("//a[contains(@href, '#product-details')]")).click();
        int qty = DataConverter.parseStockValue(
                waitElementToBeClickable(By.cssSelector("div.product-quantities>span")).getText());
        assertEquals(qty, product.getQty() - 1, "In stock value has not been decreased");
        return this;
    }
}