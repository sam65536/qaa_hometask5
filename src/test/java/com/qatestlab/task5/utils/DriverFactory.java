package com.qatestlab.task5.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.SkipException;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.openqa.selenium.UnexpectedAlertBehaviour.ACCEPT;
import static org.openqa.selenium.ie.InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS;
import static org.openqa.selenium.ie.InternetExplorerDriver.NATIVE_EVENTS;
import static org.openqa.selenium.remote.CapabilityType.ACCEPT_SSL_CERTS;

public class DriverFactory {
    /**
     *
     * @param browser Driver type to use in tests.
     * @return New instance of {@link WebDriver} object.
     */
    public static WebDriver initDriver(String browser) {
        switch (browser) {
            case "firefox":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(DriverFactory.class.getResource("/geckodriver.exe").getFile()).getPath());
                FirefoxOptions firefoxOptions  = new FirefoxOptions();
                firefoxOptions.setCapability("marionette", true);
                firefoxOptions.setUnhandledPromptBehaviour(ACCEPT);
                firefoxOptions.setCapability("acceptSslCerts", true);
                return new FirefoxDriver(firefoxOptions);
            case "ie":
            case "internet explorer":
                System.setProperty(
                        "webdriver.ie.driver",
                        new File(DriverFactory.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                InternetExplorerOptions ieOptions = new InternetExplorerOptions()
                        .requireWindowFocus()
                        .setUnhandledPromptBehaviour(ACCEPT)
                        .destructivelyEnsureCleanSession();
                ieOptions.setCapability(ACCEPT_SSL_CERTS, true);
                ieOptions.setCapability(INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                ieOptions.setCapability(NATIVE_EVENTS, false);
                return new InternetExplorerDriver(ieOptions);
            case "android":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverFactory.class.getResource("/chromedriver.exe").getFile()).getPath());
                Map<String,String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "Nexus 7");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
                return new ChromeDriver(chromeOptions);
            case "headless-chrome":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverFactory.class.getResource("/chromedriver.exe").getFile()).getPath());
                ChromeOptions options = new ChromeOptions();
                options.addArguments("headless");
                options.addArguments("window-size=800x600");
                return new ChromeDriver(options);
            case "chrome":
            default:
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverFactory.class.getResource("/chromedriver.exe").getFile()).getPath());
                return new ChromeDriver();
        }
    }

    /**
     *
     * @param browser Remote driver type to use in tests.
     * @param gridUrl URL to Grid.
     * @return New instance of {@link RemoteWebDriver} object.
     */
    public static WebDriver initDriver(String browser, String gridUrl) {
        if (gridUrl.equals("")) return initDriver(browser);
        try {
            switch (browser) {
            case "firefox":
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                return new RemoteWebDriver(new URL(gridUrl), firefoxOptions);
            case "ie":
            case "internet explorer":
                InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
                return new RemoteWebDriver(new URL(gridUrl), internetExplorerOptions);
            case "android":
                Map<String,String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "Nexus 7");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
                return new RemoteWebDriver(new URL(gridUrl), chromeOptions);
            case "headless-chrome":
                ChromeOptions remoteOptions = new ChromeOptions();
                remoteOptions.addArguments("headless");
                remoteOptions.addArguments("window-size=800x600");
                return new RemoteWebDriver(new URL(gridUrl), remoteOptions);
            default:
                ChromeOptions options = new ChromeOptions();
                return new RemoteWebDriver(new URL(gridUrl), options);
        }
            } catch (Exception e) {
                e.printStackTrace();
                throw new SkipException("Unable to create RemoteWebDriver instance!");
            }
    }
}