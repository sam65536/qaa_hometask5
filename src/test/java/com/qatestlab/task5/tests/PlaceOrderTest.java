package com.qatestlab.task5.tests;

import com.qatestlab.task5.base.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PlaceOrderTest extends BaseTest {

    @Test
    public void checkSiteVersion() {
        mainPage.launch();
        assertEquals(isMobileTesting, mainPage.isMobileSite());
    }

    @Test(priority = 1)
    public void createNewOrder() {
        mainPage.launch()
                .openRandomProduct()
                .saveOpenedProduct()
                .addProductToCart()
                .createOrder()
                .fillOrderInformation(userdata)
                .placeOrder()
                .validateOrder()
                .checkUpdatedStock();
    }
}