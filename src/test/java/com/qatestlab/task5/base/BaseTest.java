package com.qatestlab.task5.base;

import com.qatestlab.task5.pages.MainPage;
import com.qatestlab.task5.utils.DriverFactory;
import com.qatestlab.task5.utils.logging.EventHandler;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseTest {
    protected EventFiringWebDriver driver;
    protected MainPage mainPage;
    protected boolean isMobileTesting;
    protected JSONObject userdata;

    /**
     * Prepares {@link WebDriver} instance with timeout and browser window configurations.
     *
     * Driver type is based on passed parameters to the automation project,
     * creates {@link ChromeDriver} instance by default.
     *
     */
    @BeforeClass
    @Parameters({"selenium.browser", "selenium.grid"})
    public void setUp(String browser, String gridUrl) {
        driver = new EventFiringWebDriver(DriverFactory.initDriver(browser, gridUrl));
        driver.register(new EventHandler());
        driver.manage().timeouts().implicitlyWait(15, SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, SECONDS);

        if (!isMobileTesting(browser)) {
            driver.manage().window().maximize();
        }
        isMobileTesting = isMobileTesting(browser);
        mainPage = MainPage.init(driver);

        userdata = new JSONObject();
        userdata.put("firstname", "Bill");
        userdata.put("lastname", "Gates");
        userdata.put("email", "billg@microsoft.com");
        userdata.put("address1", "Seattle, WA");
        userdata.put("postcode", "20006");
        userdata.put("city", "Washington");
    }

    /**
     * Closes driver instance after test class execution.
     */
    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    /**
     *
     * @return Whether required browser displays content in mobile mode.
     */
    private boolean isMobileTesting(String browser) {
        switch (browser) {
            case "android":
                return true;
            case "firefox":
            case "ie":
            case "internet explorer":
            case "chrome":
            default:
                return false;
        }
    }
}