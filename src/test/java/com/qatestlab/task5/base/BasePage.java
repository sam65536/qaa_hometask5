package com.qatestlab.task5.base;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import static java.time.Duration.ofSeconds;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public abstract class BasePage {

    private final long TIMEOUT = 30;
    private final long FREQUENCY = 2;

    protected final WebDriver driver;
    protected Wait<WebDriver> wait;

    protected BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new FluentWait<>(driver)
                .withTimeout(ofSeconds(TIMEOUT))
                .pollingEvery(ofSeconds(FREQUENCY))
                .ignoring(StaleElementReferenceException.class)
                .ignoring(ElementNotVisibleException.class)
                .ignoring(NoSuchElementException.class);
    }

    protected WebElement waitElementPresence(final By by) {
        return wait.until(presenceOfElementLocated(by));
    }

    protected WebElement waitElementVisibility(final By by) {
        return wait.until(visibilityOfElementLocated(by));
    }

    protected WebElement waitElementToBeClickable(final By by) {
        return wait.until(elementToBeClickable(by));
    }

    protected WebElement waitElementToBeClickable(final WebElement element) {
        return wait.until(elementToBeClickable(element));
    }
}